# The first Spark Exercise 1 - Log parsing

The log can be found at:
`s3a://linkit-spark-training-data/NASA_access_log_Jul95`

Log is from a NASA Http server from 1995! It's around 2 million lines of log and each line has the following format:

`[Host]` - - `[Timestamp]` `[The HTTP request]` `[HTTP response code]` `[Size in bytes]`

example:
 
`199.72.81.55 - - [01/Jul/1995:00:00:01 -0400] "GET /history/apollo/ HTTP/1.0" 200 6245`

Tests can be run by using `sbt test`

Use `it` instead of `ignore` to enable tests.

## Good luck!