package com.mirekvink.spark

import java.time.OffsetDateTime
import java.time.format.DateTimeFormatter

import scala.util.Try
import scala.util.matching.Regex

/**
  * Simple representation of a log event
  *
  * @param host     The hostname or IP address
  * @param datetime The datetime of the event
  * @param url      The requested URL
  * @param code     The return code
  * @param size     The size of the requested object
  */
case class Log(host: String, datetime: OffsetDateTime, url: String, code: Int, size: Option[Int]) {
  // TODO: Implement
  override def toString: String = {
    """"""
  }
}

object Log {
  /**
    * Date time formatter
    */
  // TODO: Implement
  @transient val dateTimeFormatter: DateTimeFormatter = null

  /**
    * Determines ordering of the Log object
    */
  implicit def logOrdering[A <: Log]: Ordering[Log] = Ordering.by(e => e.datetime.toInstant.toEpochMilli)


  val logLineParser: Regex =
    """(.+) - - \[(.+)] "(.+)" ([0-9]+) ?([0-9]+)?(-)?""".r
  val urlParser: Regex = """[A-Z]+ ([^ ]+)($| .*$)""".r

  /**
    * Convert an raw log line into an Log case class
    *
    * @param input The raw log line
    * @return Optionally, A Log representation
    */
  // TODO: Implement
  def apply(input: String): Option[Log] = {
    ???
  }

  /**
    * Converts raw parts of a log line to a Log object
    *
    * @param host     The host that requested an URL
    * @param datetime The datetime of the event
    * @param request  The URL that was requested
    * @param rawCode  The code that was returned by the apache server
    * @param rawSize  The size of the object that was sent by apache server
    * @return Optionally, a Log object
    */
  // TODO: Implement
  private def stringToLog(host: String, datetime: String, request: String, rawCode: String, rawSize: Option[String]): Option[Log] = {
    ???
  }

  /**
    * Creates a Log object based on the given parameters
    *
    * @param host     The host that requested an URL
    * @param datetime The datetime of the event
    * @param url      The URL that was requested
    * @param code     The code that was returned by the apache server
    * @param size     The size of the object that was sent by apache server
    * @return A Log object
    */
  def apply(host: String, datetime: String, url: String, code: Int, size: Option[Int]): Log =
    Log(host, OffsetDateTime.parse(datetime, dateTimeFormatter), url, code, size)
}