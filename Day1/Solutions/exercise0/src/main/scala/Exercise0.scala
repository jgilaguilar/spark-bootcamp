import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext
import org.apache.spark.rdd.RDD

object Exercise0 {

  case class Person(age: Int, name: String)

  def main(args: Array[String]): Unit = {

    val spark = SparkSession
      .builder
      .master("local[*]") // be careful with this
      .appName("Exercise0")
      .getOrCreate()

    val groups = List((18, 25), (26, 35), (36, 45), (46, 65), (66, 200))

    val sc = spark.sparkContext

    val path = "/Users/joseaguilar/Documents/Spark_Bootcamp/exercises/Datasets/names.csv"

    val inputRDD = readCSV(sc,path)
    val people = convertToPerson(inputRDD)
    val adultAges = filterAdultPeople(people).cache()

    val result = countGroupAges_v1(groups, adultAges)
    result.foreach(println)

    val result2 = countGroupAges_v2(adultAges)
    result2.foreach(println)

    val result3 = getAverageAgePerName(adultAges)
    result3.foreach(println)

    spark.stop
  }

  /**
    * Read the file from disk
    *
    * @param sc The spark Session
    * @return A RDD with each line as String
    */
  def readCSV(sc: SparkContext, path:String): RDD[String]={
    sc.textFile(path)
  }

  /**
    * Convert the raw input into a more convenient case class, Person.
    * Hint: Use the map function and split
    *
    * @param rdd The RDD
    * @return An RDD of Person
    */
  def convertToPerson(rdd: RDD[String]): RDD[Person] = {
    rdd.map(p=>{
      val tup = p.split(",")
      Person(tup(0).toInt,tup(1))
    })
  }

  /**
    * Filter the people that are younger than 18
    *
    * @param people The RDD
    * @return An RDD of Person
    */
  def filterAdultPeople(people:RDD[Person]): RDD[Person] ={
    people
      .filter(person => person.age >= 18)
  }

  /**
    * Count the age the people that are in the range
    * Hint: Use map, filter and count
    *
    * @param groups The list of tuples with the range of ages
    * @param adultAges The RDD
    * @return Sequence with Key as the range, Value as the count
    */
  def countGroupAges_v1(groups: Seq[(Int, Int)] , adultAges: RDD[Person]): Seq[((Int, Int),Long)] ={
    groups.map {
      case (lower, upper) => ((lower, upper),adultAges.filter { person =>
        lower <= person.age && person.age <= upper
      }.count())
    }
  }

  /**
    * Transform the age into one of the 5 groups. This function is going to be used in countGroupAges_v2 function.
    * 18<=age<=25 => 1
    * 26<=age<=35 => 2
    * 36<=age<=45 => 3
    * 46<=age<=65 => 4
    * age>65 => 5
    * Hint: Use a match case
    *
    * @param age The age of the person in an Integer variable
    * @return The number of the group that the person belongs
    */
  def groupOf(age: Int): Int = {
    age match {
      case age if (age >= 18 && age <= 25) => 1
      case age if (age > 25 && age <= 35) => 2
      case age if (age > 35 && age <= 45) => 3
      case age if (age > 45 && age <= 65) => 4
      case age if age > 65 => 5
    }
  }

  /**
    * Count the age the people that are in the range using the function groupOf
    * Hint: Use map and reduceByKey
    *
    * @param adultAges The RDD
    * @return Sequence with Key as the group that belongs, Value as the count
    */
  def countGroupAges_v2(adultAges: RDD[Person]): RDD[(Int, Int)] ={
    adultAges
      .map(person => (groupOf(person.age), 1))
      .reduceByKey(_ + _)
  }

  /**
    * Get the Average age per name in the RDD
    * Hint: Use groupByKey, mapValues
    *
    * @param adultAges The RDD
    * @return An RDD of Person
    */
  def getAverageAgePerName(adultAges: RDD[Person]): RDD[(String, Int)] = {
    adultAges
      .map(p => (p.name,p.age))
      .groupByKey
      .mapValues{ iterator => iterator.sum / iterator.size}
  }

}
