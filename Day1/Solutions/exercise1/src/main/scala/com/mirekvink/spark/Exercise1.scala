package com.mirekvink.spark

import org.apache.spark.rdd.RDD
import org.apache.spark.sql.SparkSession
import org.apache.spark.SparkContext

import scala.collection.immutable.SortedSet
import java.time.temporal.ChronoUnit

object Exercise1 {

  def main(args: Array[String]): Unit = {
    val spark = initializeSpark
    val sc = spark.sparkContext

    val inputRDD = readLogFile(sc)

    val logRDD = convertToLog(inputRDD)

    val successfulRDD = extractSuccessfulRequests(logRDD)

    val countRDD = countSuccessfulRequestsPerUrl(successfulRDD)

    val top_20RDD = returnTop2OfSuccessfulRequestsPerUrl(countRDD)

    top_20RDD.foreach(println)

    val clickStreamRDD = createClickStream(logRDD)

    val clickStreamSessionRDD = createClickStreamSessions(clickStreamRDD)

    clickStreamSessionRDD.foreach(println)

    // clean termination
    spark.stop
  }

  /**
    * This will initialize required Spark settings
    */
  def initializeSpark: SparkSession = {
    val spark = SparkSession
      .builder
      .master("local[*]") // be careful with this
      .appName("Exercise1")
      .getOrCreate()
    spark
  }

  /**
    * Read the log file from disk
    *
    * @param sc The spark Session
    * @return A RDD with each line as String
    */
  def readLogFile(sc: SparkContext): RDD[String] = {
    sc.textFile("../../NASA_access_log_Jul95")
  }

  /**
    * Prints the first item of the RDD
    *
    * @param input The RDD
    */
  def printHead(input: RDD[String]): Unit = {
    input.take(1).foreach(println)
  }

  /**
    * Convert the raw input into a more convenient case class, Log.
    * Tip: Use the companion object of Log to add functions
    *
    * @param input The RDD
    * @return An RDD of Logs
    */
  def convertToLog(input: RDD[String]): RDD[Log] = {
    input.flatMap(Log(_))
  }

  /**
    * Successful requests have a certain code, use that to extract
    * only the successful requests
    *
    * @param input The RDD
    * @return An RDD with only successful requests
    */
  def extractSuccessfulRequests(input: RDD[Log]): RDD[Log] = {
    input.filter(_.code==200)
  }

  /**
    * Determine the amount of requests per url
    *
    * @param input An RDD with only successful requests
    * @return A key value pair RDD, with the key as URL and the value the hits
    */
  def countSuccessfulRequestsPerUrl(input: RDD[Log]): RDD[(String, Int)] = {
    input.map(log =>(log.url,1))
      .reduceByKey(_+_)
  }

  /**
    * Provide a top 20 of the successful requests per url, ordered descending by hits
    *
    * @param input
    * @return
    */
  def returnTop2OfSuccessfulRequestsPerUrl(input: RDD[(String, Int)]): List[(String, Int)] = {
    input.takeOrdered(20)(new Ordering[(String, Int)] {
      override def compare(x: (String, Int), y: (String, Int)): Int = y._2 - x._2
    }).toList
  }

  /**
    * Based on the hostname, a clickstream can be created. A clickstream follows a host
    * through time which urls were visted.
    * Tip: Create an implicit ordering for Log
    *
    * @param input
    * @return
    */
  def createClickStream(input: RDD[Log]): RDD[(String, SortedSet[Log])] = {
    val zero = SortedSet.empty[Log]
    val add = (acc: SortedSet[Log], log: Log) => acc + log
    val partitionAdd = (logs1: SortedSet[Log], logs2: SortedSet[Log]) => logs1 ++ logs2

    input
      .map(log => (log.host, log))
      .aggregateByKey(zero)(add, partitionAdd)
  }

  /**
    * Based on the clickstream, create sessions per host. A session lasts for 30 minutes
    * after the last visit to a url.
    *
    * @param input
    * @return A sessionized clickstream per hostname
    */
  def createClickStreamSessions(input: RDD[(String, SortedSet[Log])]): RDD[(String, Seq[Seq[Log]])] = {
    input.map{case(hostname, logs) => {
      val sessions = logs.tail.foldLeft(Seq(Seq(logs.head))){(acc, i) =>
        if(ChronoUnit.MINUTES.between(i.datetime, acc.last.last.datetime) < 30) {
          acc.init :+ (acc.last :+ i)
        } else {
          acc :+ Seq(i)
        }
      }
      (hostname, sessions)
    }}
  }


}
