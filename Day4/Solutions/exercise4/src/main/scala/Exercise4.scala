import com.amazonaws.auth.profile.ProfileCredentialsProvider
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._


object Exercise4 {

  def main(args: Array[String]): Unit = {

    // val credentials = new ProfileCredentialsProvider().getCredentials

    val spark = SparkSession
      .builder
      .master("local[*]") // Just for local test, comment this line before generating the FAT JAR
      .appName("Exercise4")
      .getOrCreate()

    import spark.implicits._

    // val sc = spark.sparkContext

    // val configuration = sc.hadoopConfiguration

    // configuration.set("fs.s3a.access.key", credentials.getAWSAccessKeyId)
    // configuration.set("fs.s3a.secret.key", credentials.getAWSSecretKey)

    val song_data_path = "s3a://linkit-spark-training-data/song_data/*/*/*/*.json" // for EMR
    // val song_data_path = "../Datasets/song_data/*/*/*/*.json"

    val log_data_path = "s3a://linkit-spark-training-data/log_data/*/*/*.json" // for EMR
    // val log_data_path = "../Datasets/log_data/*/*/*.json"

    val your_name = "linkit"

    val output_PATH = "s3a://linkit-spark-training-data/results_exercise_4/" + your_name + "/" // for EMR
    // val output_PATH = "../Datasets/results_exercise_4/" + your_name + "/"

    val songs_path = output_PATH + "songs"
    val artists_path = output_PATH + "artists"
    val time_path = output_PATH + "time"
    val users_path = output_PATH + "users"
    val songsplays_path = output_PATH + "songplays"

    val songDataDF = spark.read.json(song_data_path).filter(col("year") > 0).cache()

    val song_table_DF = songDataDF
      .select("song_id",
        "title",
        "artist_id",
        "year",
        "duration").dropDuplicates()


    song_table_DF.write.mode("overwrite")
      .partitionBy("year", "artist_id")
      .parquet(songs_path)

    val artists_table = songDataDF
      .select(
        col("artist_id"),
        col("artist_name").alias("name"),
        col("artist_location").alias("location"),
        col("artist_latitude").alias("latitude"),
        col("artist_longitude").alias("longitude")
      ).dropDuplicates()

    artists_table.write.mode("overwrite").parquet(artists_path)

    val logDataDF = spark.read.json(log_data_path).cache()

    val log_filter_DF = logDataDF
      .filter(col("page") === "NextSong")
      .filter(col("userId").isNotNull)

    val users_table_DF = log_filter_DF
      .select(
        col("userId").alias("user_id"),
        col("firstName").alias("first_name"),
        col("lastName").alias("last_name"),
        col("gender"),
        col("level")
      ).dropDuplicates()

    users_table_DF.write.mode("overwrite").parquet(users_path)

    val time_table_DF = log_filter_DF
      .withColumn("datetime", (col("ts") / 1000).cast(TimestampType))
      .withColumn("hour", hour(col("datetime")))
      .withColumn("day", dayofmonth(col("datetime")))
      .withColumn("week", weekofyear(col("datetime")))
      .withColumn("month", month(col("datetime")))
      .withColumn("year", year(col("datetime")))
      .withColumn("weekday", date_format(col("datetime"), "F"))

    time_table_DF
      .select("datetime", "hour", "day", "week", "month", "year", "weekday")
      .write.mode("overwrite")
      .partitionBy("year", "month")
      .parquet(time_path)


    val songplays_table_DF = songDataDF
      .join(log_filter_DF, songDataDF.col("artist_name") === log_filter_DF.col("artist"))
      .withColumn("songplay_id", monotonically_increasing_id())
      .withColumn("start_time", (col("ts") / 1000).cast(TimestampType))
      .select(col("songplay_id"),
        col("start_time"),
        col("userId").alias("user_id"),
        col("level"),
        col("song_id"),
        col("artist_id"),
        col("sessionId").alias("session_id"),
        col("artist_location").alias("location"),
        col("userAgent"),
        month(col("start_time")).alias("month"),
        year(col("start_time")).alias("year")
      )

    songplays_table_DF.write.mode("overwrite")
      .partitionBy("year", "month")
      .parquet(songsplays_path)


    spark.stop()
  }


}
