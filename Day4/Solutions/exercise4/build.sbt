name := "exercise4"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.4.4"

resolvers ++= Seq(
  "jitpack.io" at "https://jitpack.io",
  "Artima Maven Repository" at "http://repo.artima.com/releases",
  "Sonatype Repository" at "https://oss.sonatype.org/content/repositories/releases/",
  "Central Repository" at "http://central.maven.org/maven2/"
)

libraryDependencies ++= Seq("org.apache.spark" %% "spark-core" % sparkVersion,
  "org.apache.spark" %% "spark-sql" % sparkVersion,
  "com.amazonaws" % "aws-java-sdk" % "1.11.+",
  "org.apache.hadoop" % "hadoop-aws" % "2.8.+",
  "com.holdenkarau" %% "spark-testing-base" % "2.4.0_0.11.0" % Test,
  "org.apache.spark" %% "spark-hive" % sparkVersion % Test
)


// Now it's possible to run directly from IDE, but still does not assemble Hadoop jars into fatjar
run in Compile := Defaults.runTask(fullClasspath in Compile, mainClass in (Compile, run), runner in (Compile, run)).evaluated

fork in Test := true
javaOptions ++= Seq("-Xms512M", "-Xmx2048M", "-XX:MaxPermSize=2048M", "-XX:+CMSClassUnloadingEnabled")

parallelExecution in Test := false

assemblyOption in assembly := (assemblyOption in assembly).value.copy(includeScala = false)

assemblyMergeStrategy in assembly := {
  case "META-INF/services/org.apache.spark.sql.sources.DataSourceRegister" => MergeStrategy.concat
  case PathList("META-INF", xs@_*) => MergeStrategy.discard
  case x => MergeStrategy.first
}