import com.amazonaws.auth.profile.ProfileCredentialsProvider
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

object Exercise4 {

  def main(args: Array[String]): Unit = {

    val credentials = new ProfileCredentialsProvider().getCredentials

    val spark = SparkSession
      .builder
      .master("local[*]") // Just for local test, comment this line before generating the FAT JAR
      .appName("Exercise4")
      .getOrCreate()

    import spark.implicits._

    val sc = spark.sparkContext

    val configuration = sc.hadoopConfiguration // Just for local test, comment this line before generating the FAT JAR 

    configuration.set("fs.s3a.access.key", credentials.getAWSAccessKeyId) // Just for local test, comment this line before generating the FAT JAR 
    configuration.set("fs.s3a.secret.key", credentials.getAWSSecretKey) // Just for local test, comment this line before generating the FAT JAR 

    // val song_data_path = "s3a://linkit-spark-training-data/song_data/*/*/*/*.json" // for EMR
    val song_data_path = "../Datasets/song_data/*/*/*/*.json"


    //val log_data_path = "s3a://linkit-spark-training-data/log_data/*/*/*.json" // for EMR
    val log_data_path = "../Datasets/log_data/*/*/*.json"

    val your_name = "jose"

    val output_PATH = "s3a://linkit-spark-training-data/results_exercise_4/" + your_name + "/" // for EMR
    // val output_PATH = "../Datasets/results_exercise_4/" + your_name + "/"

    val songs_path = output_PATH + "songs"
    val artists_path = output_PATH + "artists"
    val time_path = output_PATH + "time"
    val users_path = output_PATH + "users"
    val songsplays_path = output_PATH + "songplays"




    spark.stop
  }


}
