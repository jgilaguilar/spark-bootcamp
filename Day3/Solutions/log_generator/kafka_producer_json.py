import json
import os
import argparse
from time import sleep

import random

from kafka import KafkaConsumer, KafkaProducer


import random

names = ["OLIVIA","RUBY","EMILY","GRACE","JESSICA","CHLOE","SOPHIE","LILY","AMELIA","MIA","JACK","OLIVER","THOMAS","JOSHUA","HARRY","ALFIE","CHARLIE","WILLIAM","DANIEL","JAMES"]
countries = ["ENGLAND","NETHERLANDS","BRASIL","CHINA","GERMANY","FRANCE","ARGENTINA","PORTUGAL","SPAIN","CANADA","JAPAN","BELGIUM","AUSTRALIA"]

with open("names.csv","w") as f:
    #f.write("Age,Name\n")
    for i in range(1000000):
        age = random.randrange(1,100)
        ind_name = random.randrange(0,len(names))
        name = names[ind_name]
        f.write(str(age)+","+name+"\n")


def publish_message(producer_instance, topic_name, key, value):
    try:
        key_bytes = bytes(key, encoding='utf-8')
        value_bytes = bytes(value, encoding='utf-8')
        producer_instance.send(topic_name, key=key_bytes, value=value_bytes)
        producer_instance.flush()
        print ('Message published successfully.')
    except Exception as ex:
        print ('Exception in publishing message')
        print (ex)

def connect_kafka_producer(kafka_broker):
    _producer = None
    try:
        _producer = KafkaProducer(bootstrap_servers=[kafka_broker], api_version=(0, 10))
    except Exception as ex:
        print ('Exception while connecting Kafka')
        print (ex)
    finally:
        return _producer

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="Kafka Producer")
    parser.add_argument("--topic"      , "-t", dest="topic"      , help="topic from kafka to producer messages" , type=str)
    parser.add_argument("--broker"      , "-b", dest="broker"      , help="kafka broker endpoint" , type=str)
    args = parser.parse_args()

    topic_name = args.topic
    kafka_broker = args.broker
    producer = connect_kafka_producer(kafka_broker)


    while(True):
        sleep(5)
        for i in range(25):
            data = {}
            data["Age"] = random.randrange(18,80)
            ind_name = random.randrange(0,len(names))
            data["Name"] = names[ind_name]
            ind_country = random.randrange(0,len(countries))
            data["Country"] = countries[ind_country]
            json_str = json.dumps(data)
            print(json_str)
            publish_message(producer, topic_name, 'users', json_str)

