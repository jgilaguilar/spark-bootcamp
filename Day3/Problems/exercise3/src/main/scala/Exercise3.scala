import java.text.SimpleDateFormat
import java.util.Locale
import java.sql.Timestamp

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import com.sanoma.cda.geoip.{IpLocation, MaxMindIpGeo}

import scala.util.matching.Regex

case class LogGeolocation(host: String,
                          user_id: String,
                          datetime: Timestamp,
                          req_method: String,
                          req_url: String,
                          req_protocol: String,
                          status: Int,
                          bytes: Int,
                          referrer: String,
                          user_agent: String,
                          countryCode: String,
                          countryName: String,
                          region: String,
                          city: String,
                          latitude: Double,
                          longitude: Double,
                          postalCode: String,
                          continent: String,
                          regionCode: String,
                          continentCode: String,
                          timezone: String
                          )

case class ApacheLog(host: String,
                     user_id: String,
                     datetime: Timestamp,
                     req_method: String,
                     req_url: String,
                     req_protocol: String,
                     status: Int,
                     bytes: Int,
                     referrer: String,
                     user_agent: String
                    )

case class Geolocation(countryCode: String,
                       countryName: String,
                       region: String,
                       city: String,
                       latitude: Double,
                       longitude: Double,
                       postalCode: String,
                       continent: String,
                       regionCode: String,
                       continentCode: String,
                       timezone: String
                      )

object Exercise3 {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("Exercise3")
      .master("local[*]")
      .getOrCreate()

    /**
      * The dateFormat need to be set up using the same format as the logs generated
      * The Regex Pattern is already set up, but it would be better to check the content over here
      * https://regex101.com/
      */
    val dateFormat = null
    val PATTERN = """^(\S+) (\S+) (\S+) \[([\w:/]+\s[+\-]\d{4})\] "(\S+)\s?(\S+)?\s?(\S+)?" (\d{3}|-) (\d+|-)\s?"?([^"]*)"?\s?"?([^"]*)?"?$""".r

    /**
      * These variables need to be setup from the beginning, also could be set up as parameters:
      * geo_filename: You need to download from here:
      * https://linkit-spark-training-data.s3.eu-central-1.amazonaws.com/GeoLite2-City_20191105.tar.gz
      * You need to setup the path for the file GeoLite2-City.mmdb
      *
      * input_topic: The topic from Kafka where we are going to listen, set it up with http_log
      * kafka_broker: The ip broker
      * output_topic: The topic from Kafka where we are going the send messages
      * spark_checkpoint: It is a folder that is required for Spark to send message to Kafka, it is used for fault-tolerance
      *
      */
    val geo_filename = ""

    val input_topic = "http_log"
    val kafka_broker = ""
    val output_topic = "output_log"
    val spark_checkpoint = ""

    import spark.implicits._

    /**
      * Read the stream using readStream, and subscribe the stream to the kafka topic
      *
      */

    val ds = null

    /**
      * Get the data from the stream, use the MaxMindIpGeo to get location based on the ip
      * Filter the streams that don't have location
      * Transform the data into Json, put the alias Value
      * Write the stream into the output_topic into Kafka
      */

    val df = null

    val query = null
  }

  /**
    * Get all the data from the Regex, and create an ApacheLog with the correct data type
    *
    * @param matched Get all the data from the matched Regex using group
    * @param dateFormat It will be used to get the Timestamp
    * @return
    */
  def get_apache_log(matched: Regex.Match, dateFormat: SimpleDateFormat): ApacheLog = {
    ???
  }

  /**
    * Get all the data from the iplocation, and create an Geolocation with the correct data type, if there is no
    * match with the data, fill it with null or 0 for integers
    *
    * @param iplocation Get all the data from the iplocation
    * @return
    */
  def get_data_location(iplocation: IpLocation): Geolocation = {
    ???
  }

  /**
    * Get all the data from both classes and merge it into a new one
    *
    * @param log
    * @param data_location
    * @return
    */
  def get_log_geolocation(log: ApacheLog, data_location: Geolocation): LogGeolocation = {
    ???
  }

}