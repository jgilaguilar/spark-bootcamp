import java.text.SimpleDateFormat
import java.util.Locale
import java.sql.Timestamp

import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

object Example_SSS {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder()
      .appName("Example_SSS")
      .master("local[*]")
      .getOrCreate()

    spark.sparkContext.setLogLevel("ERROR")

    val input_topic = "users"
    val kafka_broker = "192.168.0.104:9092"

    import spark.implicits._

    val ds = spark.readStream
      .format("kafka")
      .option("kafka.bootstrap.servers", kafka_broker)
      .option("subscribe", input_topic)
      .load()

    val schema = new StructType()
      .add($"Age".int)
      .add($"Name".string)
      .add($"Country".string)

    val df = ds
      .selectExpr("CAST(value AS STRING) as json")
      .select(from_json(col("json"),schema).as("data"))
      .select("data.*")


    val query = df.writeStream
      .format("console")
      .option("truncate","false")
      .start()

    query.awaitTermination()

  }
}