import com.amazonaws.auth.profile.ProfileCredentialsProvider
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._

/**
  * Original code from Mirek Vink
  *
  * Represents a zip code with information
  *
  * @param zip The zip code
  * @param city The city the zip code belongs to
  * @param loc The long/lat coordinates of this zip
  * @param pop The population of this zip
  * @param state The state this zip code is in
  */
case class Zip(zip: String, city: String, loc: Array[Double], pop: Long, state: String)

object Exercise2_2 {

  def main(args: Array[String]): Unit = {

    val credentials = new ProfileCredentialsProvider().getCredentials

    val spark = SparkSession
      .builder
      .master("local[*]")
      .appName("Exercise2_2")
      .getOrCreate()

    import spark.implicits._

    val sc = spark.sparkContext

    val configuration = sc.hadoopConfiguration

    configuration.set("fs.s3a.access.key", credentials.getAWSAccessKeyId)
    configuration.set("fs.s3a.secret.key", credentials.getAWSSecretKey)

    val zipDS = spark
      .read
      .json("s3a://linkit-spark-training-data/zips.json")
      .withColumnRenamed("_id", "zip")
      .as[Zip]

    zipDS.printSchema()

    zipDS.show(10, false)

    println("Population > 40000")

    zipDS.filter(_.pop > 40000)
      .select($"state", $"city", $"zip", $"pop")
      .orderBy(desc("pop"))
      .show(10, false)

    println("States in order of population")
    zipDS
      .select($"state", $"pop")
      .groupBy($"state")
      .sum("pop")
      .orderBy(desc("sum(pop)"))
      .show(10, false)

    println("California cities in order of population, count zip and sum pop")
    zipDS
      .filter($"state" === "CA")
      .groupBy($"city")
      .agg(count($"zip"), sum($"pop"))
      .withColumnRenamed("count(zip)", "num_zips")
      .withColumnRenamed("sum(pop)", "population")
      .orderBy(desc("population"))
      .show(10, false)

    zipDS.createOrReplaceTempView("zips_table")
    spark
      .sql("select city, COUNT(zip) AS num_zips, SUM(pop) AS population FROM zips_table WHERE state = 'CA' GROUP BY city ORDER BY SUM(pop) DESC")
      .show(10, false)

    val locAgawam = zipDS.filter(_.city == "AGAWAM").head.loc

    val distances = zipDS.filter(_.state != "HI").map(zip => {
      val d = distance(locAgawam(0), zip.loc(0), locAgawam(1), zip.loc(1))
      (zip.city, d)
    }).toDF("city", "distance")


    distances.orderBy(desc("distance")).show(10, false)


    spark.stop
  }

  /**
    * Calculates the distance between 2 points in the earth in long/lat/elevation
    *
    * @param lat1 The lat of point 1
    * @param lat2 The lat of point 2
    * @param lon1 The long of point 1
    * @param lon2 The long of point 2
    * @param el1 The elevation of point 1
    * @param el2 The elevation of point 2
    * @return The distance in meter
    */
  def distance(lat1: Double, lat2: Double, lon1: Double, lon2: Double, el1: Double = 0, el2: Double = 0): Double = {
    val R = 6371
    // Radius of the earth
    val latDistance = Math.toRadians(lat2 - lat1)
    val lonDistance = Math.toRadians(lon2 - lon1)
    val a = Math.sin(latDistance / 2) * Math.sin(latDistance / 2) + Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) * Math.sin(lonDistance / 2) * Math.sin(lonDistance / 2)
    val c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a))
    val distance = R * c * 1000
    // convert to meters
    val height = el1 - el2
    (Math.sqrt(Math.pow(distance, 2) + Math.pow(height, 2)) / 1000).toInt
  }
}
