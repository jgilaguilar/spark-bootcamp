import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types.{StringType,IntegerType,TimestampType}
import org.apache.spark.sql.expressions.Window

object Exercise2_1 {

  def main(args: Array[String]): Unit = {
    val spark = SparkSession
      .builder
      .master("local[*]")
      .appName("Exercise2_1")
      .getOrCreate()

    val path = "/Users/joseaguilar/Documents/Spark_Bootcamp/Repos/spark-bootcamp-2nd-day/Datasets/sparkify_log_small.json"

    val df = spark.read.json(path)

    df.createOrReplaceTempView("log_table")

    /*
     * Question 1
     * How many songs were played from the most played artist?
     * Find a solution with Dataframes and another one with Spark SQL
     */

    // Solution with DataFrames

    df.filter(col("artist") =!= "")
      .groupBy("artist")
      .count()
      .orderBy(desc("count"))
      .show(1)

    // Solution with Spark SQL
    spark.sql("SELECT artist, COUNT(Artist) AS count FROM log_table GROUP BY Artist ORDER BY count DESC LIMIT 1").show()

    /*
     * Question 2
     * How many female users do we have in the data set?
     * Hint: Drop duplicates
     * Find a solution with Dataframes and another one with Spark SQL
     */

    // Solution with DataFrames
     val result = df.filter(col("gender") === "F")
                    .select("userId", "gender")
                    .dropDuplicates()
                    .groupBy("gender")
                    .count()

    result.show()

    // Solution with Spark SQL
    spark.sql("SELECT COUNT(DISTINCT userID) AS COUNT_USERID FROM log_table WHERE gender = 'F'").show()

    /*
     * Question 3
     * Calculate the stadistics by hour
     * Hint:
     * Filter the column Page with the value NextSong
     * Using the column ts get the hour (Hint you need to divide the ts by 1000)
     * Group by the hour, do the count and order by
     */

    val user_log = df.withColumn("datetime", (col("ts")/1000).cast(TimestampType))
      .withColumn("hour",hour(col("datetime")))

    val songs_in_hour = user_log.filter(col("page") === "NextSong")
      .groupBy("hour")
      .count()
      .orderBy(col("hour"))

    songs_in_hour.show(30)

    /*
     * Question 4
     * Which page did user id "" (empty string) NOT visit?
     * Hint: Use a specific type of Join
     */

    // Solution with DataFrames
    val blank_pages = df.filter(df.col("userId")==="")
      .select(col("page").alias("blank_page"))
      .dropDuplicates()

    val all_pages = df.select("page")
      .dropDuplicates()

    val left_outer_join = all_pages
      .join(blank_pages,all_pages.col("page")===blank_pages.col("blank_page"),"left_outer")
      .filter(col("blank_page").isNull)
      .select("page")

    left_outer_join.show()

    // Solution with Spark SQL
    spark.sql("SELECT * FROM ( SELECT DISTINCT page FROM log_table WHERE userID='') AS user_pages RIGHT JOIN ( SELECT DISTINCT page FROM log_table) AS all_pages ON user_pages.page = all_pages.page WHERE user_pages.page IS NULL").show()

    /*
     * Question 5
     * How many songs do users listen to on average between visiting the pages 'Home' and "NextSong"
     * Hint: Use a Window Function using "userID" as partition orderBy the column "ts"
     */

    // Solution with Dataframes

    val user_window = Window.partitionBy("userID")
      .orderBy(desc("ts"))
      .rangeBetween(Window.unboundedPreceding, 0)

    val cusum = df.filter((col("page") === "NextSong") || (col("page") === "Home"))
      .select("userID", "page", "ts")
      .withColumn("homevisit", when(col("page") === "Home",1).otherwise(0))
      .withColumn("period", sum("homevisit").over(user_window))

    cusum.filter((col("page") === "NextSong"))
      .groupBy("userID", "period")
      .agg(count(col("period")))
      .agg(avg(col("count(period)")))
      .show()

    // Solution with Spark SQL

    // SELECT CASE WHEN 1 > 0 THEN 1 WHEN 2 > 0 THEN 2.0 ELSE 1.2 END;
    val is_home = spark.sql("SELECT userID, page, ts, CASE WHEN page = 'Home' THEN 1 ELSE 0 END AS is_home " +
      "FROM log_table " +
      "WHERE (page = 'NextSong') or (page = 'Home')")

    // keep the results in a new view
    is_home.createOrReplaceTempView("is_home_table")

    // find the cumulative sum over the is_home column
    val cumulative_sum = spark.sql("SELECT *, SUM(is_home) OVER (PARTITION BY userID ORDER BY ts DESC ROWS BETWEEN UNBOUNDED PRECEDING AND CURRENT ROW) AS period " +
      "FROM is_home_table")

    // keep the results in a view
    cumulative_sum.createOrReplaceTempView("period_table")

    // find the average count for NextSong
    spark.sql("SELECT AVG(count_results) FROM (" +
      "SELECT COUNT(*) AS count_results " +
      "FROM period_table " +
      "GROUP BY userID, period, page " +
      "HAVING page = 'NextSong') AS counts").show()

    spark.stop()
  }
}
